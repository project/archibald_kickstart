api = 2
core = 7.x

; Archibald
projects[archibald] = 3.13

; Archibald dependencies
projects[ctools]                = 1.12
projects[email]                 = 1.3
projects[entity]                = 1.8
projects[entityreference]       = 1.3
projects[field_collection]      = 1.0-beta12
projects[field_group]           = 1.5
projects[field_permissions]     = 1.0
projects[jquery_update]         = 2.7
projects[lang_dropdown]         = 2.5
projects[libraries]             = 2.3
projects[qtip]                  = 2.0
projects[range]                 = 1.7
projects[references_dialog]     = 1.0-beta2
projects[rules]                 = 2.10
projects[timeperiod]            = 1.0-beta1
projects[title]                 = 1.0-alpha9
projects[url]                   = 1.0
projects[views]                 = 3.16
projects[views_aggregator]      = 1.4
projects[views_bulk_operations] = 3.4
projects[views_custom_cache]    = 1.1

; Archibald patched dependencies
projects[entity_translation][version] = 1.0-beta6
projects[entity_translation][patch][] = https://www.drupal.org/files/issues/entity_translation-allow-handler-to-be-fully-extended-2750179-4.patch
projects[revisioning][version]        = 1.9
projects[revisioning][patch][]        = https://www.drupal.org/files/issues/revisioning-combined-patch--2750169-5--2070819-38.patch
projects[xautoload][version]          = 5.7
projects[xautoload][patch][]          = https://www.drupal.org/files/issues/xautoload-require_once-error-when-installing-through-drush-2802901-1_0.patch

; UX improvements
projects[admin_menu]                    = 3.0-rc5
projects[adminimal_admin_menu]          = 1.7
projects[chosen]                        = 2.1
projects[module_filter]                 = 2.0
projects[multiple_fields_remove_button] = 1.5
projects[node_clone]                    = 1.0
projects[r4032login]                    = 1.8

; Admin theme
projects[adminimal_theme][type]    = theme
projects[adminimal_theme][version] = 1.24

; Multilingual
projects[i18n]        = 1.17
projects[l10n_update] = 2.1
projects[variable]    = 2.5

; Archibald libraries
libraries[dsb-client][download][type] = get
libraries[dsb-client][download][url]  = https://github.com/educach/dsb-client/archive/0.21.0.tar.gz
libraries[dsb-client][destination]    = libraries
libraries[dsb-client][directory_name] = dsb-client

libraries[sabre-xml][download][type] = get
libraries[sabre-xml][download][url]  = https://github.com/fruux/sabre-xml/archive/1.5.0.tar.gz
libraries[sabre-xml][destination]    = libraries
libraries[sabre-xml][directory_name] = sabre-xml

libraries[sabre-uri][download][type] = get
libraries[sabre-uri][download][url]  = https://github.com/fruux/sabre-uri/archive/1.2.1.tar.gz
libraries[sabre-uri][destination]    = libraries
libraries[sabre-uri][directory_name] = sabre-uri

libraries[guzzle][download][type] = get
libraries[guzzle][download][url]  = https://github.com/guzzle/guzzle/archive/6.2.3.tar.gz
libraries[guzzle][destination]    = libraries
libraries[guzzle][directory_name] = guzzle

libraries[psr7][download][type] = get
libraries[psr7][download][url]  = https://github.com/guzzle/psr7/archive/1.4.2.tar.gz
libraries[psr7][destination]    = libraries
libraries[psr7][directory_name] = psr7

libraries[http-message][download][type] = get
libraries[http-message][download][url]  = https://github.com/php-fig/http-message/archive/1.0.1.tar.gz
libraries[http-message][destination]    = libraries
libraries[http-message][directory_name] = http-message

libraries[promises][download][type] = get
libraries[promises][download][url]  = https://github.com/guzzle/promises/archive/v1.3.1.tar.gz
libraries[promises][destination]    = libraries
libraries[promises][directory_name] = promises

libraries[vcard][download][type] = get
libraries[vcard][download][url]  = https://github.com/jeroendesloovere/vcard/archive/1.3.0.tar.gz
libraries[vcard][destination]    = libraries
libraries[vcard][directory_name] = vcard

; Archibald Curricula libraries
libraries[backbone][download][type] = get
libraries[backbone][download][url]  = https://github.com/jashkenas/backbone/archive/1.3.3.tar.gz
libraries[backbone][destination]    = libraries
libraries[backbone][directory_name] = backbone

libraries[underscore][download][type] = get
libraries[underscore][download][url]  = https://github.com/jashkenas/underscore/archive/1.8.3.tar.gz
libraries[underscore][destination]    = libraries
libraries[underscore][directory_name] = underscore

libraries[nanoScrollerJS][download][type] = get
libraries[nanoScrollerJS][download][url]  = https://github.com/jamesflorentino/nanoScrollerJS/archive/0.8.7.tar.gz
libraries[nanoScrollerJS][destination]    = libraries
libraries[nanoScrollerJS][directory_name] = nanoScrollerJS

libraries[curricula-ui][download][type] = get
libraries[curricula-ui][download][url]  = https://github.com/educach/curricula-ui/archive/1.0.0.tar.gz
libraries[curricula-ui][destination]    = libraries
libraries[curricula-ui][directory_name] = curricula-ui

; Misc libraries
libraries[chosen][download][type] = get
libraries[chosen][download][url]  = https://github.com/harvesthq/chosen/releases/download/v1.6.2/chosen_v1.6.2.zip
libraries[chosen][destination]    = libraries
libraries[chosen][directory_name] = chosen

