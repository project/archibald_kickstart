api = 2
core = 7.x

projects[drupal] = 7.59

projects[archibald_kickstart][type] = profile
projects[archibald_kickstart][download][type]   = git
projects[archibald_kickstart][download][url]    = https://git.drupal.org/project/archibald_kickstart.git
projects[archibald_kickstart][download][branch] = 7.x-3.x

; Transliterator is not GPL compatible, so it is not added in the
; drupal-org.make file. But we can add it here for our custom, local builds.
libraries[Transliterator][download][type] = get
libraries[Transliterator][download][url]  = https://github.com/Behat/Transliterator/archive/v1.2.0.tar.gz
libraries[Transliterator][destination]    = libraries
libraries[Transliterator][directory_name] = Transliterator

